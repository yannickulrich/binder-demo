# Binder examples

This is a repository containing examples for Binder that I've
collected for a short talk at IPPP Code Club on 10 May 2021. I have
four examples for the calculation of $`\nu_\mu e\to\nu_\mu e`$ using
DUNE fluxes

 * simple example, just Jupyter with `requirements.txt`
   [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/yannickulrich%2Fbinder-demo/simple-example?filepath=analysis.ipynb)

   This example is probably the minimal working example: I have some
   data, some code and a `requirements.txt` that installs `numpy` and
   `matplotlib` (Conda can also be used for this but I use `pip`).

 * Jupyter with LaTeX installed through `apt.txt`
   [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/yannickulrich%2Fbinder-demo/apt-example?filepath=analysis.ipynb)

   The image for Binder does not include LaTeX. If we want to have
   pretty labels etc. we need to install LaTeX from `apt`.

 * Hand-coded script without conversion script, launching in lab
   [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/yannickulrich%2Fbinder-demo/script-example?urlpath=lab)

   Some people, like me, prefer to work with REPL(ish) in the terminal
   (in my case combined with vim). This isn't all that great in this
   context because Binder requires a notebook.

 * Hand-coded script with conversion script
   [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/yannickulrich%2Fbinder-demo/script-conv-example)

   ..so I've written a conversion script that takes a python script
   with structuring (vim code folding) and converts it into a notebook
   that we can run through [CI](https://gitlab.com/yannickulrich/binder-demo/-/jobs/1247943574)
   and either download into Binder (or regenerate there).

Finally, I've used all of these techniques in McMule's library
[mule-tools/user-library](https://gitlab.com/mule-tools/user-library/).
